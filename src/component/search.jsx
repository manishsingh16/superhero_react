import React,{useState,useEffect} from 'react'
import "./search.css";
import axios from "axios";
//import Sres from "./sres";




const Searchh =()=>{
    const [dataa, setData] = useState();
    const [name, setName] = useState()
    const[button, setButton] = useState();
    useEffect(() => {
      axios
        .get(`https://www.superheroapi.com/api.php/103120028281050/search/${button}`)
        .then(response => setData(response.data.results))
        .catch((error) => console.log(error));        
    }, [button]);
     console.log(dataa)
    
    const handleclick=()=>{
        setButton(name);
    }

return(
    <div className="bgimage">
     <div className="searchbar">
  <input type="text" placeholder="Enter your SuperHero Name" value={name} onChange={ e => setName(e.target.value) } />
  <br/>
  <button type="button" onClick={handleclick}>Search</button>
  </div>
  <div >
          {dataa
            ? dataa.map((news) => (
                <Sres data={news} key={news.url} />
              ))
            : "Loading"}
        </div>
   </div>);
} 

const Sres=({data})=>{
    console.log(data);
      
 return(
    <div className="property">
        <div className="search-card">
            <div className="img">
                <img src={data.image.url}/>
            </div>
            <div className="search-info">
    <p>hello: {data.name}</p>
    <p>id: {data.id}</p>
    <p>Combat: {data.powerstats.combat}</p>
    <p>Durability: {data.powerstats.durability}</p>
    <p>Intelligence: {data.powerstats.intelligence}</p>
    <p>Power: {data.powerstats.power}</p>
    <p>Speed: {data.powerstats.speed}</p>
    <p>Strength: {data.powerstats.strength}</p>

 </div>
    </div>
    </div>
     
 );
 }
export default Searchh;