import React, { useContext } from 'react';
import {superheroinfo} from "./api";
import "./carddetail.css";
import CardDetail from "./cardDetail";



 
const Card = (props)=>{
  const { data } = useContext(superheroinfo);
  
  return  (
  <div className="fetchdata"><h1>SuperHero</h1>
  
  <div className="card">
  
          {data
            ? data.map((news) => (
                <CardDetail data={news} key={news.url} />
              ))
            : "Loading"}
        </div>
  </div>);
}
export default Card;
