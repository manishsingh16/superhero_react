import React from 'react';
import "./main.css";
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import bg1 from "./image/bg1.jpg"
import bg2 from "./image/bg2.jpg"
import bg3 from "./image/bg3.jpg"
import bg4 from "./image/bg5.jpg"

const Main=()=>{
    return(
        <div className="mainn">
        <div className="main">
        <div className="slideshow">
        <AliceCarousel autoPlay infinite autoPlayInterval="2000">
         <img src={bg1} className="sliderimg" alt=""/>
         <img src={bg2} className="sliderimg" alt=""/>
         <img src={bg3} className="sliderimg" alt=""/>
         <img src={bg4} className="sliderimg" alt=""/>
       </AliceCarousel>
       </div>
       </div>
       <div className="info">
           <div className="info1"><a href="https://www.marvel.com/movies">
               <h1>Marvel Cinematic Universe films</h1></a>
           <p>The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios 
               based on characters that appear in publications by Marvel Comics. The MCU is the shared universe in which all
                of the films are set. </p></div>
           <div className="info2"><a href="https://www.dccomics.com/movies"><h1>DC Universe Movies</h1></a>
           <p>DC Comics is one of the largest and oldest American comic book publishers. It produces material 
               featuring numerous well-known superhero characters, including Superman, Batman, Wonder Woman, 
               Green Lantern, The Flash, Aquaman, and Green Arrow.</p></div>
           <div className="info3"><a href="https://www.dccomics.com/comics"><h1>DC Comics</h1></a>
           <p>DC Comics, Inc. is an American comic book publisher. It is the publishing unit of DC Entertainment,
               [4][5] a subsidiary of the Warner Bros. Global Brands and Experiences division of Warner Bros.,
                a subsidiary of AT&T's WarnerMedia's Studios & Networks division. DC Comics is one of the largest 
                and oldest American comic book companies.</p></div>
           <div className="info4"><a href="https://www.marvel.com/comics?&options%5Boffset%5D=0&totalcount=12"><h1>Marvel Comics</h1></a>
           <p>Marvel Comics is the brand name and primary imprint of Marvel Worldwide Inc., formerly Marvel 
               Publishing, Inc. and Marvel Comics Group, a publisher of American comic books and related media. 
               In 2009, The Walt Disney Company acquired Marvel Entertainment, Marvel Worldwide's parent 
               company.</p></div>
       </div>
       </div>
    );
}




export default Main;