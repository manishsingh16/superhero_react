import React from 'react';
import "./footer.css";
import logo from "./image/footer-logo.jpg";
import fb from "./image/fb.jpg";
import insta from "./image/insta.jpg";
import twiter from "./image/twitter.jpg";

const Footer =()=>{
return(
    <div className="footer">
     <div className="logo"><img src={logo}/></div>
     <div className="footer-second"><p>ABOUT</p> <p>ADVERTISING</p> <p>HELP/FAQs</p> <p>CAREERS</p>   </div>
     <div className="footer-fourth"><h2>FOLLOW US</h2>
     <div class="social"><img src={fb}/> <img src={insta}/> <img src={twiter}/></div>
     </div>
    </div>
);

}
export default Footer;