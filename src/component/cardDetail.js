import React from 'react';
import "./carddetail.css";
import {Link} from "react-router-dom"

 
const CardDetail =({data})=>{
  return (
    
      <div className="imageCard">
       <Link to={{
         pathname: '/superhero/single',
         state: {data
         }
       }}><img src={data.image.url} alt="image"/></Link>
        <h3>{data.name.toUpperCase()}</h3>
        {/* <p>{data.appearance.gender}  {data.biography.alignment} {data.biography['alter-egos']}</p>
        <p>{data.biography['first-appearance']} {data.biography['full-name']} {data.biography.publisher}</p> */}

      </div>
    
    );
        

}
export default CardDetail;