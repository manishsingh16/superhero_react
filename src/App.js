import React from 'react';
import Ant from "./ant";
import Search from "./component/search";
import Headerr from "./component/header";
import Main from "./component/main";
import Footer from "./component/footer";
import Singlecard from "./component/singleCard";
import {BrowserRouter , Switch,Route } from 'react-router-dom';


const App = () =>{
    return(
        <BrowserRouter>
        <div>
        <Headerr />
        <Switch>
            <Route exact path="/" component={Main}/>
         <Route exact path="/superhero" component={Ant}/>
         <Route exact path="/search" component={Search}/>
         <Route exact path="/superhero/single" component={Singlecard}/>
         </Switch>
         <Footer />
      </div> 
      </BrowserRouter>
    );
    
}
export default App;