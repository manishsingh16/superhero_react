import React from 'react';
import ReactDom from "react-dom";
import App from "./app";
import Main from "./component/main"

ReactDom.render(<App />, document.getElementById("root"));