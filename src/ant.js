import React from 'react';
import "./ant.css";
import 'antd/dist/antd.css';
import Api from "./component/api"
import Card from "./component/card";
import { Layout} from 'antd';


const { Footer,  Content } = Layout;

const Ant =() => {
    return (
      <Layout>
      
      <Content><Api> <Card /></Api></Content>
      {/* <Footer>Footer</Footer> */}
    </Layout>

);
}

export default Ant;